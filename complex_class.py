import math

class Complex:

    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart

    def conjugate(self):
        self.i = -self.i

    def copy(self):
        return Complex(self.r, self.i)

    def __add__(self, c2):
        r = self.r + c2.r
        i = self.i + c2.i
        return Complex(r, i)  

    def __sub__(self, c2):
        r = self.r - c2.r
        i = self.i - c2.i
        return Complex(r, i)  

    def __mul__(self, c2):
        r1 = self.r * c2.r
        i1 = self.r * c2.i
        i2 = self.i * c2.r
        r2 = -(self.i * c2.i)
        return Complex(r1+r2, i1+i2)

    def __truediv__(self, c2):
        c2_conj = c2.copy()
        c2_conj.conjugate()
        l = self * c2_conj
        m = c2 * c2_conj

        r = l.r / m.r
        i = l.i / m.r
        return Complex(r, i)

    def module(self):
        return math.sqrt((self.r**2 + self.i**2))

    def __str__(self):
        info = "Real part: " + str(self.r) + "\nImag part: " + str(self.i)     
        return info   
