import os
PATH = "2_3_files_jpg_png"

files = os.listdir(PATH)
print("Before :\t", files)

for file in files:              #iterate over files in directory
    name, subtype = os.path.splitext(file)
    base = PATH + "\\" + name
    if subtype == ".png":
        os.rename(base + ".png", base + ".jpg")
    elif subtype == ".jpg":
        os.rename(base + ".jpg", base + ".png")

files = os.listdir(PATH)
print("After :\t", files)