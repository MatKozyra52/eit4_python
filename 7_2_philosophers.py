import multiprocessing
import time
import random


def wait():
    time.sleep(random.uniform(0.5, 2))

def deadlock(i, forks):
    f1 = forks[i - 1]
    f2 = forks[i]
    while 1:
        wait();                 # Wait before eating
        f1.acquire()            # Take first fork
        print(f"Philosopher {i} got fork{i - 1} ")

        wait();
        f2.acquire()            # Take secound fork
        print(f"Philosopher {i} got fork{i} ")

        print(f"Philosopher {i} is eating ")
        wait();                 # Eat

       
        f1.release()
        f2.release()
        print(f"Philosopher {i} puts down forks: {i - 1} and {i} ")


# Resource hierarchy solution
def safe(i, forks):
    if((i - 1) == -1):              # i-1 has lower priority - 0 - the highest
        f1 = forks[i]               #higher value from (0-4 range)
        f2 = forks[i-1]
    else:
        f1 = forks[i-1]             #higher priority from (0-4 range)
        f2 = forks[i]
    
    #The same loop
    while 1:
        wait();                 # Wait before eating
        f1.acquire()            # Take first fork - higher priority first
        print(f"Philosopher {i} got fork{i - 1} ")

        wait();
        f2.acquire()            # Take secound fork
        print(f"Philosopher {i} got fork{i} ")

        print(f"Philosopher {i} is eating ")
        wait();                 # Eat

        f2.release()            # Put it back - higher priority first
        f1.release()
        print(f"Philosopher {i} puts down forks: {i - 1} and {i} ")



if __name__ == '__main__':
    forks = []
    philosophers = []
    for i in range(5):
        forks.append(multiprocessing.Semaphore())
    
    mode = input("0 - deadlock; 1 - safe mode: ")
    if(mode == '0'):
        for i in range(5):
            philosophers.append(multiprocessing.Process(target=deadlock, args=(i, forks)))
            philosophers[-1].start()
    elif(mode == '1'):
        for i in range(5):
            philosophers.append(multiprocessing.Process(target=safe, args=(i, forks)))
            philosophers[-1].start()