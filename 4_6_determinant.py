import numpy as np 
from scipy import linalg

size = np.random.randint(1,100)
a = np.random.randint(0,20,(size,size))
print("a array \n", a)
det = linalg.det(a)
print("Determinant ", size, "x", size, " matrix = ", det)