to_replace = { "i": "oraz", "oraz" : "i", "nigdy":"prawie nigdy", "dlaczego":"czemu"}
input_string = "i nigdy i i i dlaczego se"
file = open("random_text_pl.txt", "r", encoding="utf-8")
input_string = file.read()
input_list = input_string.split()

output_list = [(lambda x: to_replace[x] if x in to_replace else x) (x)  for x in input_list]

output_string = ""
for word in output_list: 
    output_string += word + " "
print("== INPUT ========================================================================================")
print(input_string)
print("== OUTPUT =======================================================================================")
print(output_string)