from complex_class import Complex

dane = ''
result = Complex(0, 0)
operation = '+'
print("Prosze wpisywać kolejne liczby zespolone lub operacje jakie wykonujemy")
print("Akceptowane są wyłacznie liczby w formie zespolonej bądź operacje na nich")
print("Przykłady zapisu: \n\"3 + 22i\", \n\"22i\", \n\"3\", \n\"3 + 22i - 12i + 4 - 2\", \n\"+\", \n\"-\", \n\"*\", \n\"/\"")
print("\"=\" aby wyjść")

while dane != '=': # get input value or operation
    dane = input("->")
    splited = dane.split()

    if len(dane) > 1 or dane.isdigit():   #complex
        sign = '+'
        value = Complex(0, 0)  
        for s in splited:
            new = Complex(0, 0)
            try:
                int(s)
                new.r = int(s)
            except:
                if 'i' in s:
                    new.i = int(s.replace('i', ''))
                else:
                    sign = s
                    continue
            
            if sign == '+':
                value = value + new
            elif sign == '-':
                value = value - new
        print("Wpisana liczba: ")
        print(value) 
        if operation == '+':
            result = result + value 
        if operation == '-':
            result = result - value 
        if operation == '*':
            result = result * value 
        if operation == '/':
            result = result / value
        print("Wynik: ")
        print(result)
    else:     #operation
        operation = dane
