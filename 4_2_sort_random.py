import random

def sort_list(input_list):
    output_list = []
    buff = input_list.copy()
    for k in input_list:
        max_value_index = 0
        max_value = buff[0]
        for i, v in enumerate(buff) :
            if v > max_value:
                max_value = v
                max_value_index = i
        output_list.append(max_value)
        buff.pop(max_value_index)

    return output_list



random_list = []

for i in range(50):
    value = random.randint(0,100)
    random_list.append(value)

print("Random values: \n", random_list)
output = sort_list(random_list)
print("Sorted: \n", output)
random_list.sort(reverse=True)
print("Sorted by internal python function: \n", random_list)
print("Test if equal: \t", output == random_list)