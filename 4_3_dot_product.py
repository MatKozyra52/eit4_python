import numpy as np

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

a_arr = np.array(a)
b_arr = np.array(b)

answer = np.dot(a_arr, b_arr)

c_arr = a_arr * b_arr
dot = np.sum(c_arr)
print("dot product: ", dot)
print("Answer: ", answer)
print("Correct? :", dot == answer)
