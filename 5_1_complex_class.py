from complex_class import Complex

x = Complex(2.0, -1.0)
y = Complex(3.0, 2.0)
print("x: \n", x)
print("y: \n", y)
print("x + y \n", x+y)
print("x - y \n", x-y)
print("x * y \n", x*y)
print("x / y \n", x/y)
print("Module",x.module())


