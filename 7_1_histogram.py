import matplotlib.pyplot as plt
import numpy as np
import multiprocessing


def count_elements(i, data, out: multiprocessing.Array):
    out[i] = 0
    for k in data:
        if i == k:
            out[i]+=1
    print(i, "->", out[i])


if __name__ == '__main__':
    y = np.random.randint(0,10,2500)
    print("Random values", y)
    process = []
    amount = multiprocessing.Array("i", range(10))

    for i in range(10):
        print(amount[:])
        p = multiprocessing.Process(target=count_elements, args=(i, y, amount))
        process.append(p)
        p.start()
        print(f"Process started")

    for p in process:
        p.join()
    

    print(amount[:])
    plt.bar(range(10), amount[:])
    plt.title("Multiprocessing histogram")
    plt.xlabel("Values")
    plt.ylabel("Amount")

    plt.figure()
    plt.hist(y)
    plt.title("Build-in histogram procedure")
    plt.xlabel("Values")
    plt.ylabel("Amount")
    plt.show() 