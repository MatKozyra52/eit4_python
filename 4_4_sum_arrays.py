import numpy as np 

a = np.random.randint(0, 100, (128,128))
print("a array")
print(a)
b = np.random.randint(0, 100, (128,128))
print("b array")
print(b)
c = a+b
print("a+b array")
print(c)

c_arr = np.zeros_like(a)
for i, v in enumerate(c_arr):
    for l,m in enumerate(v):
        c_arr[i,l] = a[i,l] + b[i,l]
        
print("a+b array each element + element")
print(c_arr)
equal = (c_arr == c)
print("Is it the same?: ", np.all(equal))