input_string = "sie się se"
file = open("random_text_pl.txt", "r", encoding="utf-8")
input_string = file.read()
input_list = input_string.split()
to_remove = ( "się", "i", "oraz", "nigdy", "dlaczego")

output_list = [x for x in input_list if x not in to_remove]

output_string = ""
for word in output_list: 
    output_string += word + " "
print("== INPUT ========================================================================================")
print(input_string)
print("== OUTPUT =======================================================================================")
print(output_string)