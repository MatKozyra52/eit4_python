import json

with open('json_file.json', 'r') as openfile:
    data = json.load(openfile)

# PRINT 
print(json.dumps(data, indent = 4))


value = int(input("0. Remove 1. Add new: "))

# REMOVE
if(value == 0):
    question = "Remove element (0 - " + str(len(data)-1) + ")"
    nr = int(input(question))
    try:
        data.pop(nr)
    except:
        print("Some error with input")

    with open('json_file.json', 'w') as outputfile:
        json.dump(data, outputfile, indent = 4)


# ADD
elif( value == 1 ):
    print("Add new film to base")
    data.append({})
    data[-1]["title"] = input("Title: ")
    data[-1]["director"] = input("Directed by: ")
    data[-1]["year"] = int(input("Release year: "))
    data[-1]["rating"] = int(input("Rate (0-10): "))
    while (data[-1]["rating"] < 0 or data[-1]["rating"] > 10):
        data[-1]["rating"] = int(input("Rate (0-10): "))

    with open('json_file.json', 'w') as outputfile:
        json.dump(data, outputfile, indent = 4)
        
else:
    print("Input error!")